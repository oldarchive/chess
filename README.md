# Explanation

Chess engine that is written to conduct lessons for my curious student. It was 2020 as far as I remember.

It is not completed, because I was writing it alongside our lessons, but they ceased to continue due to his exams.

I chose the simplest algorithms I could find/come up with to be able to explain them to a child. Also, that made it possible to give him homework which he actually did, surprisingly enough.
