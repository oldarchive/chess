class GameState():
    def __init__(self):
        # TODO: Use Numpy arrays instead
        self.board = [
            ['bR', 'bN', 'bB', 'bQ', 'bK', 'bB', 'bN', 'bR'],
            ['bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp', 'bp'],
            ['--', '--', '--', '--', '--', '--', '--', '--'],
            ['--', '--', '--', '--', '--', '--', '--', '--'],
            ['--', '--', '--', '--', '--', '--', '--', '--'],
            ['--', '--', '--', '--', '--', '--', '--', '--'],
            ['wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp', 'wp'],
            ['wR', 'wN', 'wB', 'wQ', 'wK', 'wB', 'wN', 'wR']]

        self.white_to_move = True
        self.move_log = []

        self.white_king_location = (7, 4)
        self.black_king_location = (0, 4)
        self.check_mate = False
        self.stale_mate = False
        self.enpassant_move = () # coordinates for square where en passant capture is possible
        self.current_castling_right = CastleRights(True, True, True, True)
        self.castle_rights_log = [self.current_castling_right]


    # берёт ход как параметр и делает его (не работает для рокировки, превращения пешки и взятия на проходе)
    def make_move(self, move):
        self.board[move.start_row][move.start_col] = '--'
        self.board[move.end_row][move.end_col] = move.piece_moved
        self.move_log.append(move)
        self.white_to_move = not self.white_to_move # switch turns

        # update the king's location if moved
        if move.piece_moved == 'wK':
            self.white_king_location = (move.end_row, move.end_col)
        if move.piece_moved == 'bK':
            self.black_king_location = (move.end_row, move.end_col)

        # pawn promotion
        if move.is_pawn_promotion:
            self.board[move.end_row][move.end_col] = move.piece_moved[0] + 'Q'
        # enpassant_move
        if move.is_enpassant_move:
            self.board[move.start_row][move.end_col] = '--'
        # update enpassant_move variable
        # only on 2 square pawn advances
        if move.piece_moved[1] == 'p' and abs(move.start_row - move.end_row) == 2:
            self.enpassant_move = ((move.start_row + move.end_row)//2, move.start_col)
        else: 
            self.enpassant_move = ()

#         update_castle_rights(move)

    def undo_move(self):
        if len(self.move_log) != 0: # make sure there is a move to undo
            move = self.move_log.pop()
            self.board[move.start_row][move.start_col] = move.piece_moved
            self.board[move.end_row][move.end_col] = move.piece_captured
            self.white_to_move = not self.white_to_move # switch turns back

            # update the king's location if needed
            if move.piece_moved == 'wK':
                self.white_king_location = (move.start_row, move.start_col)
            if move.piece_moved == 'bK':
                self.black_king_location = (move.start_row, move.start_col)
            # undo en passant
            if move.is_enpassant_move:
                self.board[move.end_row][move.end_col] = '--' # leave landing square blank
                self.board[move.start_row][move.end_col] = move.piece_captured
                self.enpassant_move = (move.end_row, move.end_col)
            # undo a 2 square pawn advance
            if move.piece_moved[1] == 'p' and abs(move.start_row - move.end_row) == 2:
                self.enpassant_move = ()

    def update_castle_rights(self, move):
        pass


    def get_valid_moves(self):
        temp_enpassant_move = self.enpassant_move
        # 1. Generate all possible moves
        moves = self.get_possible_moves()

        for v in reversed(moves):
            self.make_move(v)
            self.white_to_move = not self.white_to_move
            if self.in_check():
                moves.remove(v)
            self.white_to_move = not self.white_to_move
            self.undo_move()

        if len(moves) == 0:
            if self.in_check():
                self.check_mate = True
            else:
                self.stale_mate = True
        # in case if we decide to undo a move which puts us under checkmate/stalemate
        else :
            self.check_mate = False
            self.stale_mate = False
        self.enpassant_move = temp_enpassant_move
        return moves

    # determine if the current player is in check
    def in_check(self):
        if self.white_to_move:
            return self.square_under_attack(self.white_king_location[0], self.white_king_location[1])
        else:
            return self.square_under_attack(self.black_king_location[0], self.black_king_location[1])


    # determine if the enemy can attack the square (r, c)
    def square_under_attack(self, r, c):
        self.white_to_move = not self.white_to_move # switch to opponent's turn
        opp_moves = self.get_possible_moves()
        self.white_to_move = not self.white_to_move # switch turns back

        for move in opp_moves:
            if move.end_row == r and move.end_col == c: # square is under attack
                return True
        return False

    def get_possible_moves(self):
        moves = [Move((6, 4), (4, 4), self.board)]
        for row in range(len(self.board)): # number of rows
            for col in range(len(self.board[row])): # number of cols in given rows
                piece_color = self.board[row][col][0]
                if (piece_color == 'w' and self.white_to_move) or (piece_color == 'b' and not self.white_to_move):
                    piece = self.board[row][col][1]
                    if piece == 'p':
                        self.get_pawn_moves(row, col, moves)
                    elif piece == 'R':
                        self.get_rook_moves(row, col, moves)
                    elif piece == 'N':
                        self.get_knight_moves(row, col, moves)
                    elif piece == 'B':
                        self.get_bishop_moves(row, col, moves)
                    elif piece == 'Q':
                        self.get_queen_moves(row, col, moves)
                    elif piece == 'K':
                        self.get_king_moves(row, col, moves)

        return moves

    # Get all the pawn moves for the pawn located at row, col and add these moves to the list
    def get_pawn_moves(self, r, c, moves):
        if self.white_to_move: # white pawn moves
            if self.board[r-1][c] == '--': # 1 square pawn advance
                moves.append(Move((r, c), (r-1, c), self.board))
                if r == 6 and self.board[r-2][c] == '--':
                    moves.append(Move((r, c), (r-2, c), self.board))
            if c-1 >= 0: # captures to the left
                if self.board[r-1][c-1][0] == 'b': # enemy piece to capture
                    moves.append(Move((r, c), (r-1, c-1), self.board))
                elif (r-1, c-1) == self.enpassant_move:
                    moves.append(Move((r, c), (r-1, c-1), self.board, is_enpassant_move=True))


            if c+1 <= 7: # captures to the right
                if self.board[r-1][c+1][0] == 'b': # enemy piece to capture
                    moves.append(Move((r, c), (r-1, c+1), self.board))
                elif (r-1, c+1) == self.enpassant_move:
                    moves.append(Move((r, c), (r-1, c+1), self.board, is_enpassant_move=True))

        else:
            if self.board[r+1][c] == '--':
                moves.append(Move((r, c), (r+1, c), self.board))
                if self.board[r+2][c] == '--' and r == 1:
                        moves.append(Move((r, c), (r+2, c), self.board))
            if c - 1 >= 0:
                if self.board[r+1][c-1][0] == 'w':
                    moves.append(Move((r, c), (r+1, c-1), self.board))
                elif (r+1, c-1) == self.enpassant_move:
                    moves.append(Move((r, c), (r+1, c-1), self.board, is_enpassant_move=True))
            if c + 1 <= 7:
                if self.board[r+1][c+1][0] == 'w':
                    moves.append(Move((r, c), (r+1, c+1), self.board))
                elif (r+1, c+1) == self.enpassant_move:
                    moves.append(Move((r, c), (r+1, c+1), self.board, is_enpassant_move=True))

    def get_knight_moves(self, r, c, moves):
        if self.white_to_move:
            all_moves = [(r-2, c-1), (r-2, c+1), (r-1, c-2), (r-1, c+2), (r+1, c-2), (r+1, c+2), (r+2, c-1), (r+2, c+1)]
            for place in all_moves:
                if not ((place[0] >= len(self.board) or place[1] >= len(self.board[0])) or (place[0] < 0  or place[1] < 0)):
                    if self.board[place[0]][place[1]] == '--' or self.board[place[0]][place[1]][0] == 'b':
                        moves.append(Move((r, c), place, self.board))

        else:
            all_moves = [(r-2, c-1), (r-2, c+1), (r-1, c-2), (r-1, c+2), (r+1, c-2), (r+1, c+2), (r+2, c-1), (r+2, c+1)]
            for place in all_moves:
                if not ((place[0] >= len(self.board) or place[1] >= len(self.board[0])) or (place[0] < 0  or place[1] < 0)):
                    if self.board[place[0]][place[1]] == '--' or self.board[place[0]][place[1]][0] == 'w':
                        moves.append(Move((r, c), place, self.board))

    def get_rook_moves(self, r, c, moves):
        enemy_color = 'b' if self.white_to_move else 'w'

        for i in range(1, 8):
            if r-i >= 0:
                end_piece = self.board[r-i][c]
                if end_piece == '--':
                    moves.append(Move((r, c), (r-i, c), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r-i, c), self.board))
                    break

        for i in range(1, 8):
            if r+i < 8:
                end_piece = self.board[r+i][c]
                if end_piece == '--':
                    moves.append(Move((r, c), (r+i, c), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r+i, c), self.board))
                    break

        for i in range(1, 8):
            if c+i < 8:
                end_piece = self.board[r][c+i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r, c+i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r, c+i), self.board))
                    break

        for i in range(1, 8):
            if c-i < 0:
                end_piece = self.board[r][c-i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r, c-i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r, c-i), self.board))
                    break

    def get_bishop_moves(self, r, c, moves):
        enemy_color = 'b' if self.white_to_move else 'w'

        for i in range(1, 8):
            if r-i >= 0 and c-i >= 0:
                end_piece = self.board[r-i][c-i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r-i, c-i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r-i, c-i), self.board))
                    break

        for i in range(1, 8):
            if r+i < 8 and c+i < 8:
                end_piece = self.board[r+i][c+i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r+i, c+i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r+i, c+i), self.board))
                    break

        for i in range(1, 8):
            if c+i < 8 and r-i >= 0:
                end_piece = self.board[r-i][c+i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r-i, c+i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r-i, c+i), self.board))
                    break

        for i in range(1, 8):
            if c-i >= 0 and r+i < 8:
                end_piece = self.board[r+i][c-i]
                if end_piece == '--':
                    moves.append(Move((r, c), (r+i, c-i), self.board))
                elif end_piece[0] == enemy_color:
                    moves.append(Move((r, c), (r+i, c-i), self.board))
                    break

    def get_queen_moves(self, r, c, moves):
        self.get_rook_moves(r, c, moves)
        self.get_bishop_moves(r, c, moves)


    def get_king_moves(self, r, c, moves):
        enemy_color = 'b' if self.white_to_move else 'w'
        allowed_king_moves = [(r-1, c), (r-1, c-1), (r-1, c+1), (r, c-1), (r, c+1), (r+1, c), (r+1, c-1), (r+1, c+1)]
        for place in allowed_king_moves:
            if 8 > place[0] >= 0 and 8 > place[1] >= 0:
                if self.board[place[0]][place[1]] == '--' or self.board[place[0]][place[1]][0] == enemy_color:
                    moves.append(Move((r, c), place, self.board))
        # Допилить рокировку

class CastleRights():
    def __init__(self, wks, bks, wqs, bqs):
        self.wks = wks
        self.bks = bks
        self.wqs = wqs
        self.bqs = bqs

class Move():
    ranks_to_rows = {'1':7, '2':6, '3':5, '4':4,
                     '5':3, '6':2, '7':1, '8':0}
    rows_to_ranks = {v: k for k, v in ranks_to_rows.items()}
    files_to_cols = {'a':0, 'b':1, 'c':2, 'd':3,
                     'e':4, 'f':5, 'g':6, 'h':7}
    cols_to_files = {v: k for k, v in files_to_cols.items()}


    def __init__(self, start_sq, end_sq, board, is_enpassant_move=False):
        self.start_row = start_sq[0]
        self.start_col = start_sq[1]
        self.end_row = end_sq[0]
        self.end_col = end_sq[1]
        self.piece_moved = board[self.start_row][self.start_col]
        self.piece_captured = board[self.end_row][self.end_col] #'--'
        self.move_id = self.start_row * 1000 + self.start_col * 100 + self.end_row * 10 + self.end_col
        # pawn promotion
        self.is_pawn_promotion = (self.piece_moved == 'wp' and self.end_row == 0) or (self.piece_moved == 'bp' and self.end_row == 7)
        # en passant
        self.is_enpassant_move = is_enpassant_move
        if self.is_enpassant_move:
            self.piece_captured = 'wp' if self.piece_moved == 'bp' else 'bp'

    def __eq__(self, other):
        if isinstance(other, Move):
            return self.move_id == other.move_id
        return False

    def get_chess_notation(self):
        return self.get_rank_file(self.start_row, self.start_col) + self.get_rank_file(self.end_row, self.end_col)

    def get_rank_file(self, row, col):
        return self.cols_to_files[col] + self.rows_to_ranks[row]
